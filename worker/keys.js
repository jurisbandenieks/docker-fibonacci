module.export = {
    redisHost: process.env.REDIS_HOST,
    redisPORT: process.env.REDIS_PORT
}